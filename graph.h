#pragma once
#include <iostream>
#include <string>
#include <queue>
#include <stack>
#include <list>
#include <limits.h>

using namespace std;

#ifndef Graph_tree
#define Graph_tree

class Node;

class Edge
{
public:
	Edge(Node *org, Node *dest, int dist)
	{
		origin = org;
		destination = dest;
		distance = dist;
	}

	Node* getOrigin() { return origin; }
	Node* getDestination() { return destination; } 
	int getDistance() { return distance; }
private:
	Node *origin;
	Node *destination;
	int distance;
};


class Node
{
public:
	Node(char id)
	{
		name = id;
	}

	void addEdge(Node *v, int dist)
	{
		Edge newEdge(this, v, dist);
		edges.push_back(newEdge); // pushback edges in to the list
	}

	void printEdges()
	{
		cout << name << ":" << endl; //print strat node + :
		for (list<Edge>::iterator it = edges.begin(); it != edges.end(); it++)
		{
			Edge e = *it;
			cout << e.getDestination()->getName() << " - " << e.getDistance() << endl; //print "destination - number of distance"
		}
		cout << endl;
	}

	char getName() { return name; } //get name
	list<Edge> getEdges() { return edges; } //get edge value

private:
	char name;
	list<Edge> edges; // list edges
};


class Graph
{
public:
	Graph() {}

	void insert(int data[4][4]) //to insert 4x4 metrix to a graph
	{
		int i = 0;
		for (int i = 0; i < 4; i++)
		{
			char name = 'A' + i; 
			Node v = Node(name);
			vertices.push_back(v); //push back to the vector
		}
		for (list<Node>::iterator it = vertices.begin(); it != vertices.end(); ++it)
		{
			for (int j = 0; j < 4; j++)
			{
				Node &g = *it;
				char name = 'A' + j;
				Node *temp = new Node(name);
				g.addEdge(temp, data[i][j]); //add edge between data
			}
			i++;
		}
	}

	void printGraph()
	{
		for (list<Node>::iterator it = vertices.begin(); it != vertices.end(); ++it)
		{
			Node &g = *it;
			g.printEdges(); //print graph "start point + destination + weight"
		}
	}

	bool pseudograph()
	{
		for (list<Node>::iterator it = vertices.begin(); it != vertices.end(); ++it)
		{
			Node &g = *it;
			list<Edge> e = g.getEdges();
			for (list<Edge>::iterator it2 = e.begin(); it2 != e.end(); ++it2)
			{
				Edge &tempE = *it2;
				Node &name = *tempE.getDestination();
				if (g.getName() == name.getName())
				{
					return true;
				}
			}
		}
		return false;
	}

	bool multigraph()
	{
		int count = 0;
		for (list<Node>::iterator it = vertices.begin(); it != vertices.end(); ++it)
		{
			Node &g = *it;
			list<Edge> e = g.getEdges();
			for (list<Edge>::iterator it2 = e.begin(); it2 != e.end(); ++it2)
			{
				Edge &tempE = *it2;
				Node &name = *tempE.getDestination();

				for (list<Edge>::iterator it3 = e.begin(); it3 != e.end(); ++it3)
				{
					Edge &tempE2 = *it3;
					Node &name2 = *tempE2.getDestination();
					if (name.getName() == name2.getName())
					{
						count++;
						if (count > 1)
						{
							return true;
						}
					}
					else
					{
						count = 0;
					}
				}
			}
		}
		return false;
	}
	bool CompleteGraph()
	{
		list<int> count;
		int temp = 0;
		bool check = true;
		for (list<Node>::iterator it = vertices.begin(); it != vertices.end(); ++it)
		{
			Node &g = *it;
			list<Edge> e = g.getEdges();
			for (list<Edge>::iterator it2 = e.begin(); it2 != e.end(); ++it2)
			{
				temp++;
			}
			count.push_back(temp);
			temp = 0;
		}
		for (list<int>::iterator num = count.begin(); num != count.end(); ++num)
		{
			if (count.begin() != num)
			{
				check = false;
			}
		}
		return check;
	}

	bool WeightedGraph()
	{
		for (list<Node>::iterator it = vertices.begin(); it != vertices.end(); ++it)
		{
			Node &g = *it;
			list<Edge> e = g.getEdges();
			for (list<Edge>::iterator it2 = e.begin(); it2 != e.end(); ++it2)
			{
				Edge &Weight = *it2;
				if (Weight.getDistance() != 0)
				{
					return true;
				}
			}
		}
		return false;
	}

	bool digraph()
	{
		for (list<Node>::iterator it = vertices.begin(); it != vertices.end(); ++it)
		{
			Node &g = *it;
			list<Edge> e = g.getEdges();
			for (list<Node>::iterator it2 = vertices.begin(); it2 != vertices.end(); ++it2)
			{
				Node &g2 = *it2;
				list<Edge> e2 = g2.getEdges();
				for (list<Edge>::iterator edges = e.begin(); edges != e.end(); ++edges)
				{
					Edge &tempE = *edges;
					Node &name = *tempE.getDestination();
					for (list<Edge>::iterator edges2 = e2.begin(); edges2 != e2.end(); ++edges2)
					{
						Edge &tempE2 = *edges2;
						Node &name2 = *tempE2.getDestination();
						if (g.getName() != g2.getName())
						{
							if (name.getName() == g2.getName() && g.getName() == name2.getName())
							{
								return true;
							}
						}
					}
				}
			}
		}
		return false;
	}

	int minDistance(int dist[], bool sptSet[])
	{
		int min = INT_MAX, min_index;

		for (int v = 0; v < 4; v++)
			if (sptSet[v] == false && dist[v] <= min)
				min = dist[v], min_index = v;

		return min_index;
	}

	void printSolution(int dist[], int n)
	{
		cout << "Vertex   Distance" << endl;
		for (int i = 0; i < 4; i++)
		{
			char name = 'A' + i;
			cout << name << "           " << dist[i] << endl;
		}
	}

	void dijkstra(int data[4][4], int src)
	{
		int dist[4];
		bool sptSet[4];
		int temp = 0;
		int F = 0;

		for (int i = 0; i < 4; i++)
		{
			dist[i] = INT_MAX, sptSet[i] = false;
		}

		dist[src] = 0;

		for (int count = 0; count < 4 - 1; count++)
		{
			int u = minDistance(dist, sptSet);

			sptSet[u] = true;
			F = 0;
			for (list<Node>::iterator it = vertices.begin(); F <= u; ++it)
			{
				if (F == u)
				{
					Node &g = *it;
					list<Edge> e = g.getEdges();
					for (int v = 0; v < 4; v++)
					{
						int S = 0;
						for (list<Edge>::iterator it2 = e.begin(); S <= v; ++it2)
						{
							Edge &tempE = *it2;
							temp = tempE.getDistance();
							S++;
						}
						if (!sptSet[v] && temp && dist[u] != INT_MAX && dist[u] + temp < dist[v])
						{
							cout << temp << endl;
							dist[v] = dist[u] + temp;
						}
					}
				}
				F++;
			}
		}

		printSolution(dist, 4);
	}

private:
	list<Node> vertices; //create list vertices
};
#endif